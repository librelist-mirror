#!/bin/ksh
set -e
set -o pipefail

LIST=$1
test -n "$LIST" || { echo >&2 "Usage: $0 LIST"; exit 1; }

# librelist.com seems to be in EST
YYYYMMDD=${YYYYMMDD-$(TZ=EST date +%Y/%m/%d)}
set -u

YYYYMM=$(expr $YYYYMMDD : '\([0-9]\{4\}/[0-9]\{2\}\)/[0-9]\{2\}$')
maildir=$LIST/$YYYYMMDD
test -d $maildir || mkdir -p $maildir

prev=$(date +%s)
sleep 1
rsync -az librelist.com::archives/$maildir/ $maildir/ 2>/dev/null || exit 0
mbox=$LIST/$YYYYMM.mbox
mbox_gz=$LIST/$YYYYMM.gz

find $LIST/$YYYYMMDD/queue/new/ -type f -printf '%p %C@\n' |
while read file ctime
do
	test $ctime -gt $prev || continue

	# this runs synchronously right now, but if not, we'll just pipe
	# it to dd with a huge obs to force an atomic write
	formail -a 'From ' < $file | gzip -c >> $mbox_gz
	formail -a 'From ' < $file >> $mbox
done
