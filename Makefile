# users are expected to create and maintain the config.mk file themselves
include config.mk

sync-all: $(LISTS)

$(LISTS):
	./librelist-archive-mbox $@

expire-all:
	find $(LISTS) -type f -ctime +1 \
	  ! -name '*.gz' ! -name '*.mbox' | xargs $(RM) -f
	find $(LISTS) -type d -ctime +1 | xargs rmdir 2>/dev/null || true

PANDOC = pandoc
PANDOC_OPTS = -s -f markdown --email-obfuscation=none --sanitize-html
pandoc = $(PANDOC) $(PANDOC_OPTS)
pandoc_html = $(pandoc) --toc -t html --no-wrap

index.html: title = $(shell sed -ne 1p < $<)
index.html: README
	$(pandoc_html) -T "$(title)" < $< > $@+
	mv $@+ $@

.PHONY: $(LISTS)
